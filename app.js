var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var config = require('./config/config');
var FacebookStrategy  =     require('passport-facebook').Strategy
var routes = require('./routes/index');
var users = require('./routes/users');
var players = require('./routes/players');
var teams = require('./routes/teams');
var lineup = require('./routes/lineup');
var GoogleAuth = require('google-auth-library');
var auth = new GoogleAuth;
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
     // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

var client = new auth.OAuth2(config.googleAuth.clientID, '', '');

app.use(function (req, res, next) {
  try {
    var authHeaderValue = req.get('Authorization');
    var token = authHeaderValue.replace('Bearer: ', '');
    console.log(token);  
  } catch (err) {
    res.status(404)
    .json({
          status: 'ERROR',
          message: 'Please include an authorization token with all requests'
        });
        return false;
  }
  client.verifyIdToken(
    token,
    config.googleAuth.clientID,
    function(e, login) {
      try {
        if (e) {
          res.status(404)
          .json({
            status: 'ERROR',
            message: 'Google user not authenticated ' + e
          });  
          return false;
        }
        var payload = login.getPayload();
      } catch (err) {
        res.status(404)
        .json({
          status: 'ERROR',
          message: 'Google user not authenticated ' + err
        });
        return false;
      }
      
      var aud_authentic = payload['aud'] === config.googleAuth.clientID ? true : false;
      var iss_authentic = payload['iss'] === 'https://accounts.google.com' ? true: false;
      var nowDate = new Date();
      var exp_authentic = (payload['exp']*1000) > nowDate ? true: false;
      console.log('one more time');
      if (aud_authentic && iss_authentic && exp_authentic) {
        
        
        app.locals.google_user = {};
        app.locals.google_user =  {
            sub: payload['sub'],
            email: payload['email']
          }
        next();

      } else {
        res.status(404)
        .json({
          status: 'ERROR',
          message: 'Google user not authenticated'
        });
        return false;
      }
      
      // If request specified a G Suite domain:
      //var domain = payload['hd'];
    });
});

app.use('/', routes);
app.use('/api/', users);
app.use('/api/players', players);
app.use('/api/teams', teams);
app.use('/api/lineup', lineup);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
