var promise = require('bluebird');
var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var cn = {
    password: 'password',
    database: 'baseballapi',
    port: 5432,
    max: 10, 
    };
var db = pgp(cn);

// add query functions
function getAllPlayers (req, res, next) {
    db.any('select * from players')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL players'
        });
    })
    .catch(function (err) {
        console.log('aint nobody to return');
      return next(err);
    });
}

// get players by team
function getPlayersByTeam(req, res, next) {
  var team_id = parseInt(req.params.id);
  db.any('select * from players where team_id = $1', team_id)
    .then(function (data) {
      res.status(200)
        .json({
          data
        })
    })
    .catch(function (err) {
      return next(err);
    })
}

function getSinglePlayer(req, res, next) {
    var player_id = parseInt(req.params.id);
      db.one('select * from players where id = $1', player_id)
        .then(function (data) {
          res.status(200)
            .json({
              status: 'success',
              data: data,
              message: 'Retrieved ONE player'
            });
        })
        .catch(function (err) {
          return next(err);
    });
}

function createPlayer(req, res, next) {
  var values = createPlayerString(req.body);
  var sql = 'insert into players (' + values[0]   + ')' +
    ' values (' + values[1]  + ') returning player_id';
    console.log(sql);
  db.one(sql)
  .then(function (data) {
    res.status(200)
      .json({
        status: 'success',
        data: data.id,
        message: 'Inserted one player'
      });
  })
  .catch(function (err) {
    console.log(err);
    return res.status(404)
      .json({
        status: 'error',
        message: err
      });
  });   
}

function updatePlayer(req, res, next) {
    var body = parseRequest(req);
    console.log('body: ' + body);
     db.none('update players set ' + body + ' where id=$1',
    [parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated player'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
}

function updateMultiplePlayers(req, res, next) {
  
    var values = createMultiplePlayerUpdate(req.body);
    var sql = 'UPDATE players as p set\
    singles = c.singles, \
    doubles = c.doubles,\
    triples = c.triples,\
    home_runs = c.home_runs,\
    hits = c.hits\
 from (values ' + values + '\
) as c(player_id, singles, doubles, triples, home_runs, hits) \
where c.player_id = p.player_id';

     db.none(sql)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated player'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removePlayer(req, res, next) {
    var playerID = parseInt(req.params.id);
  db.result('delete from players where id = $1', playerID)
    .then(function (result) {
      /* jshint ignore:start */
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} player`
        });
      /* jshint ignore:end */
    })
    .catch(function (err) {
      return next(err);
    });
}

function parseRequest(req) {
  
  var str = '';
  var valid_str = ['first_name', 'last_name']
  var valid_int = ['at_bats', 'walks', 'hits', 'doubles', 'triples', 'home_run', 'runs','rbis', 'steals', 'attempted_steals']
  var obj = req.body;
  for (var prop in obj) {
    if (valid_int.indexOf(prop) > -1) {
      str = str + prop+"="+obj[prop] + ",";
    }
    if (valid_str.indexOf(prop) > -1) {
      str = str + prop+"="+ "'" + obj[prop] +"'"+ ",";
    }
  }
  str = str.slice(0, -1);
  return str;
}

function createMultiplePlayerUpdate(data) {
  var string = '';
  for (var x in data) {
    var player = data[x];
    var str = '(' + player.player_id + ', ' + player.singles + ', ' + player.doubles + ', ' + player.triples + ', ' + player.home_runs + ', ' + player.hits + '),'
    string += str;
  }
  string = string.slice(0,  -1);
  return string;
}

function createPlayerString(data) {
  var query_str = '';
  var value_str = '';
  var str_arr = [];
  var valid_str = ['first_name', 'last_name']
  var valid_int = ['team_id', 'at_bats', 'walks', 'hits', 'doubles', 'triples', 'home_runs', 'runs','rbis', 'steals', 'attempted_steals']
  var obj = data;
  for (var prop in obj) {
    if (valid_int.indexOf(prop) > -1) {
      query_str = query_str  + prop + ', ';
      value_str = value_str + obj[prop] + ', ' ;
    }
    if (valid_str.indexOf(prop) > -1) {
      query_str = query_str + prop + ', ' ;
      value_str = value_str + '\'' + obj[prop] + '\', ' ;
    }
  }
  str_arr[0] = query_str.slice(0, -2);
  str_arr[1] = value_str.slice(0, -2);
  return str_arr;
}
module.exports = {
  getAllPlayers: getAllPlayers,
  getPlayersByTeam: getPlayersByTeam,
  getSinglePlayer: getSinglePlayer,
  updateMultiplePlayers: updateMultiplePlayers,
  createPlayer: createPlayer,
  updatePlayer: updatePlayer,
  removePlayer: removePlayer
};