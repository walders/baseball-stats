var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var cn = {
    password: 'password',
    database: 'baseballapi',
    port: 5432,
    max: 10, 
    };
var db = pgp(cn);

// add query functions
function getAllUsers (req, res, next) {
    db.any('select * from users')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL users'
        });
    })
    .catch(function (err) {
        console.log('aint nobody to return');
      return next(err);
    });
}

function getSingleUser(req, res, next) {
    var user_id = parseInt(req.params.id);
      db.one('select * from users where user_id = $1', user_id)
        .then(function (data) {
          res.status(200)
            .json({
              status: 'success',
              data: data,
              message: 'Retrieved ONE user'
            });
        })
        .catch(function (err) {
          return next(err);
    });
}

function createUser(req, res, next) {
  console.log('please tell me something: ' + JSON.stringify(req.app.locals.google_user));
  var user_name = req.app.locals.google_user.email;
  var sql = 'WITH upsert AS (\
  UPDATE users SET role=$2, password=$3\
  WHERE username=$1 \
  RETURNING users.*\
  ), inserted AS ( \
  INSERT INTO users (username, role, password) \
  SELECT $1, $2, $3 WHERE NOT EXISTS( SELECT * FROM upsert ) \
  RETURNING * \
  ) \
  select * \
  from upsert \
  union all \
  select * \
  from inserted';
  db.one(sql, [user_name, 'manager', 'password'])
  
  .then(function (data) {
      checkIfUserWasCreated(data, res);
  })
  .catch(function (err) {
    console.log('We got an error: ' + JSON.stringify(err));
    return res.status(404)
      .json({
        status: 'error',
        message: err.detail
      });
  });   
}

function checkIfUserWasCreated(data, res) {
  console.log('We are in checkIfUserWasCreated. ' + data.user_id);
  if (data && data.user_id) {
    db.one('SELECT t.team_id, u.user_id FROM users u LEFT JOIN teams t ON t.user_id = u.user_id WHERE u.user_id = $1', data.user_id)
    .then(function (data) {
        return res.status(200)
        .json({
          status: 'success',
          data: data
          })
      })
    .catch(function (err) {
    //console.log(err.detail);
    return res.status(404)
      .json({
        status: 'error',
        message: err
      });
  }); ;
  } 
}
function updateUser(req, res, next) {
     db.none('update user set parent_id=$1, username=$2, password=$3, role=$4 where id=$5',
    [parseInt(req.body.parent_id), req.body.username, req.body.password,
      req.body.role, parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated user'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removeUser(req, res, next) {
    var userID = parseInt(req.params.id);
  db.result('delete from user where id = $1', userID)
    .then(function (result) {
      /* jshint ignore:start */
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} user`
        });
      /* jshint ignore:end */
    })
    .catch(function (err) {
      return next(err);
    });
}
module.exports = {
  getAllUsers: getAllUsers,
  getSingleUser: getSingleUser,
  createUser: createUser,
  updateUser: updateUser,
  removeUser: removeUser
};