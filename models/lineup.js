var promise = require('bluebird');
var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var cn = {
    password: 'password',
    database: 'baseballapi',
    port: 5432,
    max: 10, 
    };
var db = pgp(cn);

// add query functions
function getLineupById (req, res, next) {
    var id = parseInt(req.params.id);
    db.any('select * from lineup WHERE lineup_id = $1', id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved lineup by id'
        });
    })
    .catch(function (err) {
        console.log('aint nobody to return');
      return next(err);
    });
}

// get players by team
function getLineupsByTeam(req, res, next) {
  var team_id = parseInt(req.params.id);
  db.any('select * from lineup where team_id = $1', team_id)
    .then(function (data) {
      res.status(200)
        .json({
          data
        })
    })
    .catch(function (err) {
      return next(err);
    })
}

function getLineupsByGame(req, res, next) {
    var game_id = parseInt(req.params.id);
      db.one('select * from lineup where game_id = $1', game_id)
        .then(function (data) {
          res.status(200)
            .json({
              status: 'success',
              data: data,
              message: 'Retrieved lineup by game'
            });
        })
        .catch(function (err) {
          return next(err);
    });
}

function createLineup(req, res, next) {
  var sql = 'insert into lineup(game_id, team_id, lineup)' +
    'values(${game_id}, ${team_id}, ${lineup}) returning id';
  db.one(sql,
  req.body)
  .then(function (data) {
    res.status(200)
      .json({
        status: 'success',
        data: data.id,
        message: 'Inserted one lineup'
      });
  })
  .catch(function (err) {
    return res.status(404)
      .json({
        status: 'error',
        message: err
      });
  });   
}

function updateLineup(req, res, next) {
    var body = parseRequest(req);
     db.none('update lineup set ' + body + ' where id=$1',
    [parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated lineup'
        });
    })
    .catch(function (err) {
      console.log(err);
      return next(err);
    });
}

function updateMultiplePlayers(req, res, next) {
     db.none('update players set parent_id=$1, playername=$2, password=$3, role=$4 where id=$5',
    [parseInt(req.body.parent_id), req.body.playername, req.body.password,
      req.body.role, parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated player'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removeLineup(req, res, next) {
    var id = parseInt(req.params.id);
  db.result('delete from lineup where lineup_id = $1', id)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          data: result,
          message: `Removed ${result.rowCount} player`
        });
      /* jshint ignore:end */
    })
    .catch(function (err) {
      return next(err);
    });
}

function startGameAndLineup(req, res, next) {
    
    var team_id = parseInt(req.params.id);
    var lineup_data = req.body;
    console.log('what do we have here:  ' + typeof lineup_data);
    console.log(lineup_data);
    db.one('WITH x AS ( INSERT INTO game (home_team_id, in_progress) VALUES ($1, true) RETURNING home_team_id, game_id ),y AS (INSERT INTO lineup (team_id, game_id, lineup) SELECT x.home_team_id, x.game_id, $2:json FROM  x RETURNING * ) SELECT * FROM y;', [team_id, lineup_data])
        .then(function (result) {
            
            res.status(200)
                .json({
                    status: 'success',
                    data: result,
                    message: ''
                })
        })
        .catch(function (err) {
    console.log(err);
    return res.status(404)
      .json({
        status: 'error',
        message: err
      });
  });   
    
}

function parseRequest(req) {
  
  var str = '';
  var valid_str = ['lineup'];
  var valid_int = ['lineup_id', 'game_id', 'team_id'];
  var obj = req.body;
  for (var prop in obj) {
    if (valid_int.indexOf(prop) > -1) {
      str = str + prop+"="+obj[prop] + ",";
    }
    if (valid_str.indexOf(prop) > -1) {
      str = str + prop+"="+ "'" + obj[prop] +"'"+ ",";
    }
  }
  str = str.slice(0, -1);
  return str;
}
module.exports = {
  getLineupById: getLineupById,
  getLineupsByTeam: getLineupsByTeam,
  getLineupsByGame: getLineupsByGame,
  updateMultiplePlayers: updateMultiplePlayers,
  startGameAndLineup: startGameAndLineup,
  createLineup: createLineup,
  updateLineup: updateLineup,
  removeLineup: removeLineup
};