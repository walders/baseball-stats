var express = require('express');
var router = express.Router();
var db = require('../models/players');

router.get('/', db.getAllPlayers);
router.get('/:id', db.getSinglePlayer);
router.get('/team/:id', db.getPlayersByTeam);
router.post('/', db.createPlayer);
router.put('/:id', db.updatePlayer);
router.put('/updateMultiple/:id', db.updateMultiplePlayers);
router.delete('/:id', db.removePlayer);


module.exports = router;