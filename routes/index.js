var express = require('express');
var router = express.Router();
var pg = require('pg');
var path = require('path');
var players = require('../models/players');
var test = process.env.baseballapi;
//var connectionString = require(path.join(__dirname, '../', '../', 'config'));

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/v1/insert_players', function(req, res) {
/*    
    var pool = new pg.Pool({
    password: 'password',
    database: 'baseballapi',
    port: 5432,
    max: 10, 
    idleTimeoutMillis: 30000,});
*/
    var data = {first_name: req.body.first_name, last_name: req.body.last_name};
/*
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      client.query('INSERT INTO players(first_name, last_name) values($1, $2)', [data.first_name, data.last_name],  function(err, result) {
        //call `done()` to release the client back to the pool
        done();
    
        if(err) {
          return console.error('error running query', err);
        }
        console.log(result.rows[0]);
        //output: 1
      });
    });
*/
    if (!req.body.first_name) {
        res.render('index', {title: 'There was a problem with the thing.'})
    } else {
        players.insertPlayer(data, function (err, player){
            res.render('index', { title: 'The player ' + player.first_name + ' was inserted.' });    
            
        });
              
    }
  
});

/* Get players by name */

router.get('/api/v1/players/getName', function(req, res) {
    var data = {first_name: req.body.first_name, last_name: req.body.last_name};
    console.log(req.body);
    if (!req.body.first_name) {
        res.render('index', {title: 'There was a problem with the thing.'})
    } else {
        players.getPlayers(data, function (err, player){
            res.json(player)
        });
              
    }
})

/* Add player. */
router.post('/api/v1/players', function(req, res) {
    console.log('we here');
    var results = [];
    if (!req) {
        res.render('index', {title: 'There was a problem with the thing.'})
    }
    // Grab data from http request
    var data = {first_name: req.body.first_name, last_name: req.body.last_name};

    // Get a Postgres client from the connection pool
    pg.connect(test, function(err, client, done) {
        // Handle connection errors
        if(err) {
          done();
          console.log(err);
          return res.status(500).json({ success: false, data: err});
        }

        // SQL Query > Insert Data
        client.query("INSERT INTO players(first_name, last_name) values($1, $2)", [data.text, data.complete]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });


    });
});

module.exports = router;
