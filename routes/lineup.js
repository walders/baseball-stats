var express = require('express');
var router = express.Router();
var db = require('../models/lineup');


router.get('/:id', db.getLineupById);
router.get('/team/:id', db.getLineupsByTeam);
router.get('/game/:id', db.getLineupsByGame);
router.post('/', db.createLineup);
router.post('/team/:id', db.startGameAndLineup);
router.put('/:id', db.updateLineup);
router.delete('/:id', db.removeLineup);


module.exports = router;